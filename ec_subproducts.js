fixPermutations = function() {
  $(".permutation").each(function () {
    this.checked = this.value;
  });
}

attachCheckAllPermutations = function () {
  $(".check-all").each(function() {
    $(".nojs").empty();
    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.id = this.id;
    checkbox.checked = 1;
    checkbox.onclick = function() {
      $(".permutation").each(function() {
        this.checked = checkbox.checked;
      });
    }
    $(this).append(checkbox);
  });
  
}

if (Drupal.jsEnabled) {
  $(document).ready(fixPermutations).ready(attachCheckAllPermutations);
}

