Product Module  : EC_SUBPRODUCTS
Original Author : Nedjo Rogers
Settings        : > administer > store > settings > ec_subproducts

********************************************************************
DESCRIPTION:

TBA.

********************************************************************

See MAINTAINERS.txt for more maintenance info.

This was greatly modified and updated by Gordon Heydon.

Ported from Ecommerce 3 and Drupal 5 to EC 4 Drupal 6 by
Peter Forgacs (foripepe)
http://drupal.org/node/338865

See README.txt (in E-Commerce root) for other info.
