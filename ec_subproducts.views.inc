<?php

/**
 * @file
 */

/**
 * Implementation og hook_views_data().
 */
function ec_subproducts_views_data() {
  $data = array();
  
  $data['ec_product']['pparent'] = array(
    'title' => t('Is parent product'),
    'help' => t('Filter out nodes which are parent products'),
    'filter' => array(
      'field' => 'pparent',
      'handler' => 'ec_subproducts_views_handler_filter_pparent',
      'label' => t('Is parent'),
      'type' => 'yes-no',
    ),
    'relationship' => array(
      'title' => t('Parent Product'),
      'help' => t('link to any child product nodes.'),
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Parent Product'),
    ),
  );
  
  $data['ec_product_attribute']['table']['group'] = t('Product');
  $data['ec_product_attribute']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
      'type' => 'LEFT',
    ),
  );
  
  $data['ec_attribute']['table']['group'] = t('Product');
  $data['ec_attribute']['table']['join'] = array(
    'node' => array(
      'left_table' => 'ec_product_attribute',
      'left_field' => 'aid',
      'field' => 'aid',
    ),
  );
  $data['ec_attribute']['aid'] = array(
    'title' => t('Attribute'),
    'help' => t('Product attribute'),
    'filter' => array(
      'handler' => 'ec_subproducts_views_handler_filter_aid'
    ),
  );
  $data['ec_attribute']['vid'] = array(
    'title' => t('Variation'),
    'help' => t('Product Variation'),
    'filter' => array(
      'handler' => 'ec_subproducts_views_handler_filter_vid',
    ),
  );
  $data['ec_attribute']['name'] = array(
    'title' => t('Attribute'),
    'help' => t('Product variation attribute'),
    'field' => array(
      'label' => t('Attribute'),
    )
  );
  
  $data['ec_variation']['table']['group'] = t('Product');
  $data['ec_variation']['table']['join'] = array(
    'node' => array(
      'left_table' => 'ec_attribute',
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );
  $data['ec_variation']['name'] = array(
    'title' => t('Variation'),
    'help' => t('Product Variation'),
    'field' => array(
      'label' => t('Variation'),
    ),
  );
  
  $data['ec_product_child']['table']['group'] = t('Product');
  $data['ec_product_child']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'table' => 'ec_product',
      'field' => 'pparent',
    ),
  );
  $data['ec_product_child']['nid'] = array(
    'title' => t('Child products'),
    'help' => t('Join to any child products of the parent product.'),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Child products'),
    ),
  );
  
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function ec_subproducts_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ec_subproducts') . '/views',
    ),
    'handlers' => array(
      'ec_subproducts_views_handler_filter_pparent' => array(
        'parent' => 'views_handler_filter_boolean_operator'
      ),
      'ec_subproducts_views_handler_filter_aid' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'ec_subproducts_views_handler_filter_vid' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}