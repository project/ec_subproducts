<?php

/**
 * @file
 *  Features for sub-products.
 */

/**
 * Implementation of hook_admin_form().
 */
function ec_subproducts_features_admin_form(&$form_state, $info) {
  $form = array();
  
  if (empty($info->variations)) {
    $info->variations = array();
  }
  
  ctools_include('object-cache');
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();
  
  if ($object = ctools_object_cache_get('feature-edit', $info->ptype . '-' . $info->ftype)) {
    $info = $object;
  }
  else {
    ctools_object_cache_set('feature-edit', $info->ptype . '-' . $info->ftype, $info);
  }

  $type = ec_product_get_types('type', $info->ptype);
  $weight = $type->features['anonymous']->weight-1;
  
  $form['weight'] = array(
    '#type' => 'value',
    '#value' => $weight,
  );
  $form['variations'] = array(
    '#type' => 'value',
    '#value' => $info->variations,
  );
  
  $form['table'] = array(
    '#tree' => TRUE,
    '#theme' => 'ec_subproducts_features_admin_form_variations',
  );

  $parents = array_map('_ec_subproducts_features_admin_form_map_parents', $info->variations);

  foreach ($info->variations as $vid => $variation) {
    if (!isset($variation->delete) || !$variation->delete) {
      $form['table'][$vid] = array(
        'vid' => array(
          '#type' => 'value',
          '#value' => $vid,
        ),
        'variation_id' => array(
          '#type' => 'hidden',
          '#value' => $vid,
        ),
        'name' => array(
          '#value' => $variation->name,
        ),
        'parent' => array(
          '#type' => 'hidden',
          '#value' => 0,
        ),
        'weight' => array(
          '#type' => 'weight',
          '#default_value' => $variation->weight,
        ),
        'operations' => array(
          '#value' => ctools_modal_text_button(t('edit'), 'ec-subproducts/nojs/variation/' . $info->ptype . '/' . $vid . '/edit', t('Edit variation')) . ' ' . ctools_ajax_text_button(t('delete'), 'ec-subproducts/nojs/variation/' . $info->ptype . '/' . $vid . '/delete', t('Delete variation')) . ' ' . ctools_modal_text_button(t('add attribute'), 'ec-subproducts/nojs/attributes/' . $info->ptype . '/' . $vid . '/add', t('Add attribute')),
        ),
      );

      if (!empty($variation->attributes)) {
        foreach ($variation->attributes as $aid => $attribute) {
          if (!isset($attribute->delete) || !$attribute->delete) {
            $form['table'][$vid]['attributes'][$aid] = array(
              'aid' => array(
                '#type' => 'value',
                '#value' => $aid,
              ),
              'variation_id' => array(
                '#type' => 'hidden',
                '#value' => $vid . '-' . $aid,
              ),
              'name' => array(
                '#value' => $attribute->name,
              ),
              'vid' => array(
                '#type' => 'select',
                '#default_value' => $attribute->vid,
                '#options' => $parents,
              ),
              'weight' => array(
                '#type' => 'weight',
                '#default_value' => isset($attribute->weight) ? $attribute->weight : 10,
              ),
              'operations' => array(
                '#value' => ctools_modal_text_button(t('edit'), 'ec-subproducts/nojs/attributes/' . $info->ptype . '/' . $vid . '/' . $aid . '/edit', t('Edit attribute')) . ' ' . ctools_ajax_text_button(t('delete'), 'ec-subproducts/nojs/attributes/' . $info->ptype . '/' . $vid . '/' . $aid . '/delete', t('Delete attribute')),
              ),
            );
          }
        }
      }
    }
  }
  
  $form['add_variation'] = array(
    '#value' => ctools_modal_text_button(t('Add variation'), 'ec-subproducts/nojs/variation/' . $info->ptype . '/add', t('Add variation')),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  return $form;
}

function ec_subproducts_features_admin_form_submit(&$form, &$form_state) {
  $variations = $form_state['values']['variations'];

  foreach ($form_state['values']['table'] as $vid => $variation) {
    $variations[$vid]->weight = $variation['weight'];

    if (isset($form_state['values']['table'][$vid]['attributes'])) {
      foreach ($form_state['values']['table'][$vid]['attributes'] as $aid => $attribute) {
        $variations[$vid]->attributes[$aid]->weight = $attribute['weight'];
        if ($vid != $attribute['vid']) {
          $variations[$vid]->attributes[$aid]->vid = $attribute['vid'];
          $variations[$attribute['vid']]->attributes[$aid] = $variations[$vid]->attributes[$aid];
          unset($variations[$vid]->attributes[$aid]);
        }
      }
    }
  }

  form_set_value($form['variations'], $variations, $form_state);

  ctools_object_cache_clear('feature-edit', $form_state['values']['ptype'] . '-' . $form_state['values']['ftype']);
}

function ec_subproducts_features_admin_form_cancel(&$form, &$form_state) {
  ctools_object_cache_clear('feature-edit', $form_state['values']['ptype'] . '-' . $form_state['values']['ftype']);
}

function _ec_subproducts_features_admin_form_map_parents($a) {
  return $a->name;
}

function theme_ec_subproducts_features_admin_form_variations($form) {
  $output = '';
  
  $head = array(t('Name'), t('Weight'), t('Operations'));
  $rows = array();
  
  foreach (element_children($form) as $vid) {
    $form[$vid]['variation_id']['#attributes']['class'] = 'ec-subproducts-vid';
    $form[$vid]['parent']['#attributes']['class'] = 'ec-subproducts-parent-vid';
    $form[$vid]['weight']['#attributes']['class'] = 'ec-subproducts-weight';
    $rows[] = array(
      'data' => array(
        drupal_render($form[$vid]['variation_id']) . drupal_render($form[$vid]['name']),
        drupal_render($form[$vid]['parent']) . drupal_render($form[$vid]['weight']),
        drupal_render($form[$vid]['operations']),
      ),
      'class' => 'draggable tabledrag-root',
    );

    if (isset($form[$vid]['attributes'])) {
      foreach (element_children($form[$vid]['attributes']) as $aid) {
        $form[$vid]['attributes'][$aid]['weight']['#attributes']['class'] = 'ec-subproducts-weight';
        $form[$vid]['attributes'][$aid]['vid']['#attributes']['class'] = 'ec-subproducts-parent-vid';
        $form[$vid]['attributes'][$aid]['variation_id']['#attributes']['class'] = 'ec-subproducts-vid';

        $rows[] = array(
          'data' => array(
            theme('indentation', 1) . drupal_render($form[$vid]['attributes'][$aid]['variation_id']) . drupal_render($form[$vid]['attributes'][$aid]['name']),
            drupal_render($form[$vid]['attributes'][$aid]['vid']) . drupal_render($form[$vid]['attributes'][$aid]['weight']),
            drupal_render($form[$vid]['attributes'][$aid]['operations']),
          ),
          'class' => 'draggable tabledrag-leaf',
        );
      }
    }
  }
  
  if ($rows) {
    drupal_add_tabledrag('ec-subproducts-variations', 'match', 'parent', 'ec-subproducts-parent-vid', 'ec-subproducts-parent-vid', 'ec-subproducts-vid');
    drupal_add_tabledrag('ec-subproducts-variations', 'order', 'sibling', 'ec-subproducts-weight');
    $output .= theme('table', $head, $rows, array('id' => 'ec-subproducts-variations'));
  }
  else {
    $output .= '<div id="ec-subproducts-variations"></div>';
  }
  
  return $output;
}

function ec_subproducts_features_variations_edit($js, $op, $ptype, $vid = NULL) {
  ctools_include('form');
  ctools_include('object-cache');
  ctools_include('ajax');
  ctools_include('modal');
  
  $commands = array();
  
  $info = ctools_object_cache_get('feature-edit', $ptype . '-subproducts');
  
  if ($op == 'add') {
    $variation = new stdClass;
    $variation->name = '';
    $variation->weight = 10;
    $variation->ptype = $ptype;
  }
  else {
    $variation = $info->variations[$vid];
  }
  
  $form_state = array(
    'no_redirect' => TRUE,
    'args' => array(
      ec_product_get_types('type', $ptype),
      $variation,
    ),
    'ajax' => $js,
  );
  
  $output = ctools_modal_form_wrapper('ec_subproducts_features_variation_form', $form_state);
  
  if (empty($output)) {
    if ($form_state['clicked_button']['#id'] != 'cancel') {
      if ($op == 'add') {
        $info->variations[] = $variation;
        $keys = array_keys($info->variations);
        $vid = array_pop($keys);
      }
      foreach ($form_state['values'] as $key => $value) {
        $info->variations[$vid]->{$key} = $value;
      }
    
      ctools_object_cache_set('feature-edit', $ptype . '-subproducts', $info);
    }
    if ($js) {
      $commands[] = ctools_modal_command_dismiss();
      if ($form_state['clicked_button']['#id'] != 'cancel') {
        module_load_include('inc', 'ec_product', 'ec_product.admin');
        $fs = array(
          'no_redirect' => TRUE,
          'args' => array($op, $ptype, 'subproducts'),
          'ajax' => $js,
          'want form' => TRUE,
        );
        $form = ctools_build_form('ec_product_admin_ptypes_feature_edit', $fs);
        
        $commands[] = ctools_ajax_command_replace('#ec-subproducts-variations', drupal_render($form['table']));
      }
    }
    else {
      drupal_goto('admin/ecsettings/products/types/' . $ptype . '/subproducts/edit');
    }
  }
  else {
    $commands = $output;
  }
  
  if ($js) {   
    ctools_ajax_render($commands);
  }
  else {
    return $output;
  }
}

/**
 * Returns a form for adding a variation.
 *
 * @param $edit
 *   Associative array containing a variation be added or edited.
 * @return
 *   String representation of editing form.
 */
function ec_subproducts_features_variation_form(&$form_state, $ptype, $variation = NULL) {
  if (!isset($variation)) {
    $variation = (object)array(
      'ptype' => $ptype->ptype,
      'name' => '',
      'weight' => 0,
    );
  }

  drupal_set_title(t('Add variation for @title', array('@title' => $ptype->name)));

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Variation name'),
    '#default_value' => $variation->name,
    '#size' => 50,
    '#maxlength' => 64,
    '#description' => t('The variation name is used to identify a group of attributes.'),
    '#attributes' => NULL,
    '#required' => TRUE,
  );
  $form['ptype'] = array(
    '#type' => 'value',
    '#value' => $variation->ptype,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#id' => 'cancel',
  );
  if (isset($variation->vid)) {
    $form['vid'] = array(
      '#type' => 'value',
      '#value' => $variation->vid,
    );
  }

  return $form;
}

function ec_subproducts_features_variations_delete($js, $ptype, $vid) {
  ctools_include('form');
  ctools_include('object-cache');
  ctools_include('ajax');
  
  $commands = array();

  $info = ctools_object_cache_get('feature-edit', $ptype . '-subproducts');
  $variation = $info->variations[$vid];

  $info->variations[$vid]->delete = TRUE;

  ctools_object_cache_set('feature-edit', $ptype . '-subproducts', $info);

  if ($js) {
    module_load_include('inc', 'ec_product', 'ec_product.admin');
    $fs = array(
      'no_redirect' => TRUE,
      'args' => array($op, $ptype, 'subproducts'),
      'ajax' => $js,
      'want form' => TRUE,
    );
    $form = ctools_build_form('ec_product_admin_ptypes_feature_edit', $fs);
    
    $commands[] = ctools_ajax_command_replace('#ec-subproducts-variations', drupal_render($form['table']));
    ctools_ajax_render($commands);
  }
  else {
    drupal_goto('admin/ecsettings/products/types/' . $ptype . '/subproducts/edit');
  }
}

function ec_subproducts_features_attributes_edit($js, $op, $ptype, $vid, $aid = NULL) {
  ctools_include('form');
  ctools_include('object-cache');
  ctools_include('ajax');
  ctools_include('modal');
  
  $commands = array();
  
  $info = ctools_object_cache_get('feature-edit', $ptype . '-subproducts');
  $variation = $info->variations[$vid];
  if ($op == 'add') {
    $attribute = new stdClass;
    $attribute->name = '';
    $attribute->surcharge = 0;
    $attribute->ptype = $ptype;
    $attribute->vid = $vid;
    $attribute->weight = 10;
  }
  else {
    $attribute = $variation->attributes[$aid];
  }
  
  $form_state = array(
    'no_redirect' => TRUE,
    'args' => array(
      ec_product_get_types('type', $ptype),
      $info->variations,
      $attribute,
    ),
    'ajax' => $js,
  );
  
  $output = ctools_modal_form_wrapper('ec_subproducts_features_attribute_form', $form_state);
  
  if (empty($output)) {
    if ($form_state['clicked_button']['#id'] != 'cancel') {
      if ($op == 'add') {
        $info->variations[$vid]->attributes[] = $attribute;

        $keys = array_keys($info->variations[$vid]->attributes);
        $aid = array_pop($keys);
      }
      foreach ($form_state['values'] as $key => $value) {
        $info->variations[$vid]->attributes[$aid]->{$key} = $value;
      }

      ctools_object_cache_set('feature-edit', $ptype . '-subproducts', $info);
    }
    if ($js) {
      $commands[] = ctools_modal_command_dismiss();
      if ($form_state['clicked_button']['#id'] != 'cancel') {
        module_load_include('inc', 'ec_product', 'ec_product.admin');
        $fs = array(
          'no_redirect' => TRUE,
          'args' => array($op, $ptype, 'subproducts'),
          'ajax' => $js,
          'want form' => TRUE,
        );
        $form = ctools_build_form('ec_product_admin_ptypes_feature_edit', $fs);
        
        $commands[] = ctools_ajax_command_replace('#ec-subproducts-variations', drupal_render($form['table']));
      }
    }
    else {
      drupal_goto('admin/ecsettings/products/types/' . $ptype . '/subproducts/edit');
    }
  }
  else {
    $commands = $output;
  }

  if ($js) {
    ctools_ajax_render($commands);
  }
  else {
    return $output;
  }
}

/**
 * Returns a form for adding an attribute.
 *
 * @param $edit
 *   Associative array containing an attribute to be added or edited.
 * @return
 *   String representation of editing form.
 */
function ec_subproducts_features_attribute_form(&$form_state, $ptype, $variations, $attribute) {
  drupal_set_title(t('Add attribute for @title', array('@title' => $ptype->name)));

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Attribute name'),
    '#default_value' => $attribute->name,
    '#size' => 50,
    '#maxlength' => 64,
    '#description' => t('The name is used to identify the attribute.'),
    '#attributes' => NULL,
    '#required' => TRUE,
  );
  $form['surcharge'] = array(
    '#type' => 'textfield',
    '#title' => t('Surcharge'),
    '#default_value' => $attribute->surcharge ? $attribute->surcharge : '0.00',
    '#size' => 25,
    '#maxlength' => 50,
    '#description' => t('What is the amount added to the price of a product of this attribute?'),
    '#validate_element' => array('valid_price'),
  );
  $form['ptype'] = array(
    '#type' => 'value',
    '#value' => $ptype->ptype,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#id' => 'cancel',
  );
  if (isset($attribute->aid)) {
    $form['aid'] = array(
      '#type' => 'value',
      '#value' => $attribute->aid,
    );
  }

  return $form;
}

function ec_subproducts_features_attributes_delete($js, $ptype, $vid, $aid = NULL) {
  ctools_include('form');
  ctools_include('object-cache');
  ctools_include('ajax');
  
  $commands = array();

  $info = ctools_object_cache_get('feature-edit', $ptype . '-subproducts');
  $variation = $info->variations[$vid];
  $attribute = $variation->attributes[$aid];

  $info->variations[$vid]->attributes[$aid]->delete = TRUE;

  ctools_object_cache_set('feature-edit', $ptype . '-subproducts', $info);

  if ($js) {
    module_load_include('inc', 'ec_product', 'ec_product.admin');
    $fs = array(
      'no_redirect' => TRUE,
      'args' => array($op, $ptype, 'subproducts'),
      'ajax' => $js,
      'want form' => TRUE,
    );
    $form = ctools_build_form('ec_product_admin_ptypes_feature_edit', $fs);
    
    $commands[] = ctools_ajax_command_replace('#ec-subproducts-variations', drupal_render($form['table']));
    ctools_ajax_render($commands);
  }
  else {
    drupal_goto('admin/ecsettings/products/types/' . $ptype . '/subproducts/edit');
  }
}


/**
 * Implementation of hook_attributes().
 */
function ec_subproducts_features_attributes($node, $attribute) {
  if (is_string($node) || !isset($node->pparent) || !$node->pparent) {
    return array('use_product_cart_form' => TRUE, 'product_form_additional_fields' => TRUE, 'no_buynow' => TRUE);
  }
  else {
    return array('use_product_cart_form' => FALSE, 'no_cart' => TRUE, 'no_buynow' => TRUE);
  }
}

/**
 * Set the matching ec_product for a set of product variation attributes.
 *
 * This function should be called in _productapi hooks with op = 'cart add item'
 * for products supporting ec_subproducts with variations. When called, the
 * function will search for a matching ec_product and, if found, set the
 * $node object to that ec_product (rather than the parent product). Since
 * the node is passed by reference, the cart module will receive a valid
 * ec_product reference.
 *
 * @param $node
 *   A node object representing a parent product.
 * @param $type
 *   Product type.
 * @param $qty
 *   Product quantity.
 * @param $data
 *   An object with properties representing, in this case, ec_product attributes.
 * @return
 *   Boolean indicating success or failure in setting a ec_product.
 */
function ec_subproducts_features_ec_checkout_validate_item(&$node, $type, $qty, $data, $return, $severity) {
  static $products = array();

  $data_defaults = array('variations' => array());
  if ($node->pparent == 0 && !$data) {
    if (empty($products[$node->nid])) {
      $result = db_query('SELECT n.nid, epa.aid, ea.vid FROM {node} n INNER JOIN {ec_product} ep ON n.vid = ep.vid INNER JOIN {ec_product_attribute} epa ON ep.nid = epa.nid INNER JOIN {ec_attribute} ea ON ea.aid = epa.aid WHERE ep.pparent = %d', $node->nid);
      while ($row = db_fetch_object($result)) {
        $products[$node->nid][$row->nid][$row->vid] = $row->aid;
      }
    }
    
    foreach ($products[$node->nid] as $nid => $product) {
      $data_copy = array();
      $data_copy['variations'] = $product;
      //if (ec_subproducts_features_ec_checkout_validate_item($node, $type, $qty, $data_copy, $return, EC_VALIDATE_ITEM_SEVERITY_LOW)) {
      if (ec_checkout_validate_item($node, $type, $qty, $data_copy, EC_VALIDATE_ITEM_SEVERITY_LOW)) {
        return TRUE;
      }
    }
    if ($severity > EC_VALIDATE_ITEM_SEVERITY_MEDIUM) {
      drupal_set_message(t('Unable to purchase this product.'));
    }
    return FALSE;
  }
  elseif (!$data) {
    $data = $data_defaults;
  }
  elseif (is_array($data)) {
    $data+= $data_defaults;
  }
  
  // ec_cart_get_cart(ec_checkout_get_id(), $item_id)
  // will be NULL if an addition is taking place, from Gordon
  $item = ec_cart_get_cart(ec_checkout_get_id(), $node->nid);
  if (!(isset($item))) {
    return TRUE;
  }

  // if there are no children then we need to return
  if(empty($node->children)) {
    return TRUE;
  }

  // this still has an arg() call in it, it will not last long!
  if ((arg(0) == 'cart') && (!(isset($item))) && $node->pparent && !$data['variations']) {
    // We already know the product.
    return TRUE;
  }

  // if this is a parent product from a form them it is ok.
  if ($node->pparent == 0 && $type == 'ec_checkout_product_form') {
    return TRUE;
  }
  
  if ($nid = ec_subproducts_get_variations_ec_product($node->nid, $data['variations'])) {
    $node = node_load($nid);
    return TRUE;
  }
  else {
    if ($severity > EC_VALIDATE_ITEM_SEVERITY_MEDIUM) {
      drupal_set_message(t('Unable to find the correct product based upon the variation combination.'));
    }
    return FALSE;
  }
}

/**
 * Implementation of hook_ptype_load().
 */
function ec_subproducts_features_ptype_load(&$info) {
  $info->variations = ec_subproducts_get_variations($info->ptype);
}

/**
 * Implementation of hook_ptype_save().
 */
function ec_subproducts_features_ptype_save(&$info) {
  foreach ($info['variations'] as $variation) {
    if (isset($variation->delete) && $variation->delete) {
      unset($variation->name);
    }
    if (isset($variation->vid) || isset($variation->name)) {
      $variation = (object)ec_subproducts_save_variation((array)$variation);

      foreach ($variation->attributes as $attribute) {
        $attribute->vid = $variation->vid;
        if (isset($attribute->delete) && $attribute->delete) {
          $attribute->name = NULL;
        }
        if (isset($attribute->aid) || $attribute->name) {
          $attribute = ec_subproducts_save_attribute((array)$attribute);
        }
      }
    }
  }
}
