<?php

/**
 * @file
 * Implement handler for filtering by product attributes
 */

class ec_subproducts_views_handler_filter_aid extends views_handler_filter_in_operator {
  function has_extra_options() { return TRUE; }
  
  function extra_options_form(&$form, &$form_state) {
    $options = array();
    
    $result = db_query('SELECT vid, name, ptype FROM {ec_variation} ORDER BY weight, name');
    while ($variation = db_fetch_object($result)) {
      $options[$variation->vid] = $variation->name;
    }
    
    $form['variation'] = array(
      '#type' => 'radios',
      '#title' => t('Variations'),
      '#default_value' => $this->options['variation'],
      '#options' => $options,
    );
  }
  
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Attributes');
      $this->value_options = array();
      $result = db_query('SELECT a.aid, a.name FROM {ec_attribute} a WHERE a.vid = %d ORDER BY a.weight, a.name', $this->options['variation']);
      while ($attribute = db_fetch_object($result)) {
        $this->value_options[$attribute->aid] = $attribute->name;
      }
    }
  }
}