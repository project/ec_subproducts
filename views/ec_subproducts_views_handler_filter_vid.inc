<?php
/**
 * @file
 * Provide filter for attribute type
 */

class ec_subproducts_views_handler_filter_vid extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Variations');
      $this->value_options = array();
      $result = db_query('SELECT vid, name FROM {ec_variation} ORDER BY weight, name');
      while ($variarion = db_fetch_object($result)) {
        $this->value_options[$variarion->vid] = $variarion->name;
      }
    }
  }
}