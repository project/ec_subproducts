<?php

/**
 * @file
 * Provide filter for product nodes.
 */

class ec_subproducts_views_handler_filter_pparent extends views_handler_filter_boolean_operator {
  function query() {
    $table = $this->ensure_my_table();
    if ($this->value) {
      $this->query->add_where($this->options['group'], "$table.pparent = 0");
    }
    else {
      $this->query->add_where($this->options['group'], "$table.pparent");
    }
  }
}
